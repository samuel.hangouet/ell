libELL: a single header library to Embed LL grammar and parser as C++ code.

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2005-2021 Samuel Hangouët

This library is written for quick developpement of LL(n) parsers, or other kind
of [EBNF](http://en.wikipedia.org/wiki/EBNF)-guided C++ applications.

It allows to write EBNF grammars in an executable manner, directly as C++ code.

# Features

  * Everything to write a parser in a ~1200 lines single header
  * Extremely fast with zero-allocation and very few virtual function calls
  * Automatically generated and understandable error messages
  * No need to use a separate lexer
  * Easy to debug and maintain

# Quick start

To illustrate the use of libELL, let's implement a simple calculator.

You can look at the [full compiling exemple here](https://gitlab.com/samuel.hangouet/ell/-/blob/master/tests/Calculator.h).

## Inlining an ENBF grammar

Using a commonly used [EBNF notation](https://www.w3.org/TR/REC-xml/#sec-notation)
notation, we can define how our mathematic expressions work:

```EBNF
expression = term ( "+" term
                  | "-" term ) *;

term = factor ( "*" factor
              | "/" factor ) *;

factor = "-" factor
       | "+" factor
       | "(" expression ")"
       | REAL;
```

Capital words and quoted characters are tokens, ie. the terminators of the
grammar. The `*` operator means zero or more repetition.

Here below is the grammar translated into C++ using libELL:

```C++
expression = term >> *( ch('+') >> term
                      | ch('-') >> term );

term = factor >> *( ch('*') >> factor 
                  | ch('/') >> factor );

factor = ch('-') >> factor
       | ch('+') >> factor
       | ch('(') >> expression >> ch(')')
       | real;
```

The main difference is the use of operator `>>` to express a sequence.

To keep the spirit of a BNF grammar, we jump to a new line on introducing an
alternative with the `|` operator. We can also use parenthesis to avoid the use
of an intermediate rule.

## The ell::Grammar class

You can put thi code in the constructor of a class extendeing `ell::Grammar`:

```C++
struct Calculator : public ell::Grammar
{
    // Build the calculator grammar
    Calculator()
    {
        expression = term >> *( ch('+') >> term .action([&]{ binop<'+'>(); })
                              | ch('-') >> term .action([&]{ binop<'-'>(); }) );

        term = factor >> *( ch('*') >> factor .action([&]{ binop<'*'>(); })
                          | ch('/') >> factor .action([&]{ binop<'/'>(); }) );

        factor = ch('-') >> factor .action([&]{ push(-pop()); })
               | ch('+') >> factor
               | ch('(') >> expression >> ch(')')
               | real.action<double>([&](double n){ push(n); });
    }

    ell::Rule expression, term, factor;
};
```

## Debug your grammar

To understand how parsing works, you can use the libELL trace. To enable it, sets the
`Parser::showTrace\ variable:

```C++
parser.showTrace = true;
```

The recursive exploration of branches is then printed in stderr with a dump of
the current position in input buffer:
  * `{`: parser starts trying the rule
  * `}=`: parser succeeded at matching the rule
  * `}x`: the rule did not match the input buffer

The use of brackets is usefull to navigate in the trace using a good editor.

Here is the trace output with the previous calculator example:

```
> 42  
{  expression 	"42"
 {  term 	"42"
  {  factor 	"42"
   {  '-' 	"42"
   }x '-' 	"42"
   {  '+' 	"42"
   }x '+' 	"42"
   {  '(' 	"42"
   }x '(' 	"42"
   {  real 	"42"
   }= real 	""
  }= factor 	""
  {  *('*' or '/') 	""
   {  '*' 	""
   }x '*' 	""
   {  '/' 	""
   }x '/' 	""
  }= *('*' or '/') 	""
 }= term 	""
 {  *('+' or '-') 	""
  {  '+' 	""
  }x '+' 	""
  {  '-' 	""
  }x '-' 	""
 }= *('+' or '-') 	""
}= expression 	""
{  end 	""
}= end 	""
```

In this example, we see that the position of parsing in the buffer remains before `42` until
the rule `real` matches. Indeed, a not matching rule never consumes any character in the 
buffer being parsed.

If you want parsing performance, do not forget to disable the trace (by passing `-DELL_TRACE=0` 
to your favorite compiler for example).

This code produces a parsing tree whose noes all have a `match` function which takes
an instance of the ell::Parser class:

```C++
bool NodeBase::match(Parser * p) const;
```

Every node knows the type of its child, which means the compiler can aggressively inline the code.
Only rules produce a virtual function call, to enable recursive grammars.

The table below presents the list of material provided by this class:

| Kind      | Name                   | Meaning |
|:----------|:-----------------------|:--------|
| primitive | `any`                  | any input character |
| primitive | `ch('X')`              | particular character |
| primitive | `chset`                | a set of characters given in regexp style (ex: `chset("-A-Za-z0-9_")`) |
| primitive | `integer<Base,Min,Max,Type>()` | integer number with given base, number of characters and result storage type |
| primitive | `dec`                  | equivalent to `integer<10,1-1,int>()` |
| primitive | `udec`                 | equivalent to `integer<10,1-1,unsigned int>()` |
| primitive | `eos`                  | matches the end of the input (but does not consume it) |
| primitive | `eps`                  | epsilon: always matches without consuming any character |
| primitive | `nop`                  | never matches not consumes anything |
| primitive | `range`                | characters range |
| primitive | `real`                 | a floating point number like parsed by `strtod`) |
| primitive | `str(s)`               | matches the given string |
| primitive | `istr(s)`              | matches the given string, ignoring case |
| primitive | `error("msg")`         | Raise error with the given message |
| binary    | `a \| b`                | `a` or `b` (if `a` mathes, `b` not evaluated |
| binary    | `a >> b`               | `a` then `b`, raise an error if `a` matches but not `b` unless look-ahead is enabled |
| binary    | `a - b`                | matches `a` unless `b` has first matched |
| binary    | `a % b`                | matches a list of at least one `a` separated by `b`. Any trailing `b` is not consumed. |
| binary    | `nosfx(a, suffix)`     | `a` not followed by anything matching `suffix` |
| binary    | `skip(skipper, a)`     | ignore anything matching `skipper` before matching any other node in `a` subtree |
| unary     | `a[var]`               | if `a` matches, assign the result into `var` (taken by reference) |
| unary     | `a.action<type>(lambda)` | if `a` matches, calls `lambda` passing the result of match as `type` in parameter (default void). If `lambda` returns false, does not match |
| unary     | `repeat<Min,Max>(a)`   | repeat n times with `Min <= n <= Max`. `Max=-1` means inifinite |
| unary     | `! a`                  | optional, equivalent to `repeat<0,1>(a)` |
| unary     | `+ a`                  | repeat at leat once, equivalent to `repeat<1,-1>(a)` |
| unary     | `* a`                  | zero of more, equivalent to `repeat<0,-1>(a)` |
| unary     | `fallback(a)`          | when the right node of an `>>` operator does not match, tries next branch instead of raising an error |
| unary     | `check(a)`             | try to match `a` and restore the position in the input buffer whatever the result |
| unary     | `noskip(a)`            | disable skipping for `a` subtree |
| unary     | `token(a)`             | equivalent to `fallback(no_skip(a))` |

## Adding semantic actions

Now, we can add semantic actions, ie. actions executed when a grammar node
matches.  To compute the operations in recursive manner, we will need a stack:

```C++
struct Calculator : public ell::Grammar
{
    Calculator()
    {
        expression = term >> *( ch('+') >> term.action([this]{ auto [a,b] = pop2(); push(a+b); })
                              | ch('-') >> term.action([this]{ auto [a,b] = pop2(); push(a-b); }) );

        term = factor >> *( ch('*') >> factor.action([this]{ auto [a,b] = pop2(); push(a*b); })
                          | ch('/') >> factor.action([this]{ auto [a,b] = pop2(); push(a/b); }) );

        factor = ch('-') >> factor.action([this]{ push(-pop()); })
               | ch('+') >> factor
               | ch('(') >> expression >> ch(')')
               | real.action<double>([this](double n){ push(n); });
    }

    // Pops from the stack
    double pop()
    {
        double n = stack.back();
        stack.pop_back();
        return n;
    }

    // Pushes into the stack
    void push(double n)
    {
        stack.push_back(n);
    }
    
    // Pops two operands from the stack
    std::pair<double,double> pop2()
    {
        double n2 = stack.back(); stack.pop_back();
        double n1 = stack.back(); stack.pop_back();
        return {n1,n2};
    }

    std::vector<double> stack;
    ell::Rule expression, term, factor;
};
```

## Errors handling

Now, we can parse an expression and output the result. Error messages are automatically
generated using the grammar, we only have to catch ell::Error exception:

```C++
int main(int, char**)
{
    Calculator calc;
    ell::Parser parser;
    std::cout << "> ";
    std::string cmd;
    std::getline(std::cin, cmd);
    try
    {
        parser.match(calc.expression, "<stdin>", cmd.c_str(), cmd.size());
    }
    catch (ell::Error & e)
    {
        std::cerr << e.what();
        return 1;
    }
    std::cout << pop() << "\n";
    return 0;
}
```

Now if we enter an incomplete expression, we get this:
```
> 1+
<stdin>:1:3: error: expecting '-' or '+' or '(' or real before end
```

To have fancier error messages, we can give a name to some rules:
```C++
    expression.setName("expression");
    term.setName("term");
    factor.setName("factor");
```

Instead of detailing the expected terminals, the name of the rule is now used:
```
> 1+
<stdin>:1:3: error: expecting term before end
```

To make sure that every character is consumed, and no trailing character
remains after the parsed expression, you can use the end-of-string (`eos`) node
in a new `root` rule:

```C++
root = expression >> eos;
```

Then, any not consumed trailing character will also trigger an error message:
```
> 4*5@
<stdin>:1:4: error: expecting end before "@"
```

To choose error messages, you can directly call the `Parser::raise_error` function,
or simply use the `error` special node:

```C++
    root = expression >> (eos | error("trailing character at the end of input"));
```

## Space skipping

To tolerate spaces inside a expression, you just have to add a `skip` modifier at root:

```C++
blank = chset(" \t\n\r");

root = skip(blank, expression >> eos);
```

But you may need to disable skipper in some part of the grammar, for example to define
variable identifiers. This is done thanks to the `no_skip` modifier. With the following
lines of grammar, we match a word possibly written in UTF-8 format (in a tolerant manner):

```C++
first_char = chset("a-zA-Z_") | range(0x80, 0xFF);

next_char = chset("a-zA-Z0-9_") | range(0x80, 0xFF);

identifier = no_skip(first_char >> *next_char);
```


## Prevent branch conflicts

The idea behind libELL is that simplicity is better that heavy-featuring: 
writing a grammar is no more than building up a tree along
which the Parser will perform a depth-first search.

So, order does matter when writing a grammar. Consider the following (quite
naïve) exemple:

```
rule1 = str("foo") >> str("bar")
      | str("foo") >> str("l");
```

If you try to parse the word `fool`, the parser will first try to match the
first branch of the `|`-alternative. The second primitive `str("bar")` won't
match, so the parser will raise an error (or fall back on the second branch,
if you used the `fallback` modifier.)

A better way to write the grammar would be :

```
rule1 = str("foo") >> ( str("bar")
                      | str("t") );
```

Even in look-ahead mode, this will prevent the parser from stepping-back to a
token which has already been consumed.

## LL(n) and look-ahead considerations

Initially, libEll was made to parse LL(1) grammars, i.e. grammars where knowing
which is the next token is sufficiant to decide which branch to use.  But in
some particular grammar, you need to consume one or more terminal before
knowing if you are in the right branch.

For that you can use the either `check` or `fallback` modifiers.

`fallback` modifier disables error raising within the sequence operator (`>>`).
In this case, the parser will fallback on next branches, when an error is
encountered. 

To keep your grammar simpler, faster, and error message more understandable,
avoid use of `fallback` whenever you can.  If you still need it, never put it
at the root of the grammar, but limit its usage for basic sub-rules. Most of
the time, you will need it at lexical level, to disambiguate terminals, in this
case, the short-hand `token` can be useful to disable space skiping at the same
time.

Another way to check more than the next token to decide which branch should match
is the use of `check` modifier. This simply restore the parser position in
the input buffer whatever the match result.

## Not ported features from old libELL version

The old libELL had other features like:
  - unary node to match without applying semantic actions
  - unary node to enable the trace for a particular sub-tree
  - use of operator [] to call parser method

After rewriting the libELL for C++20, it appears that thoses feature were not usefull anymore,
but if you need them for your grammar, feel free to contribute to libELL or contact me.

