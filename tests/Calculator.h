#include <ell/Grammar.h>

// Full calculator example
struct Calculator : public ell::Parser, public ell::Grammar
{
    // Build the calculator grammar
    Calculator()
    {
        root = skip(blank, expression >> (eos | error("trailing character at the end of input")));

        expression = term >> *( ch('+') >> term.action([this]{ auto [a,b] = pop2(); push(a+b); })
                              | ch('-') >> term.action([this]{ auto [a,b] = pop2(); push(a-b); }) );

        term = factor >> *( ch('*') >> factor.action([this]{ auto [a,b] = pop2(); push(a*b); })
                          | ch('/') >> factor.action([this]{ auto [a,b] = pop2(); push(a/b); }) );

        factor = ch('-') >> factor.action([this]{ push(-pop()); })
               | ch('+') >> factor
               | ch('(') >> expression >> ch(')')
               | real.action<double>([this](double n){ push(n); });

        blank = chset(" \t\n\r");

        expression.setName("expression");
        term.setName("term");
        factor.setName("factor");
    }

    double eval(const std::string & expr)
    {
        stack.clear();
        match(root, "<stdin>", expr.c_str(), expr.size());
        return pop();
    }

    // Read/Eval/Print loop in standard streams
    void loop(bool throwExceptions)
    {
        while (true)
        {
            // Print prompt
            std::cout << "\n> ";
            std::string cmd;

            // Get expression to parse
            std::getline(std::cin, cmd);
            if (std::cin.eof())
                break;

            // Parse and evaluate the results at the same time
            if (throwExceptions)
            {
                match(root, "<stdin>", cmd.c_str(), cmd.size());
            }
            else
            {
                try
                {
                    match(root, "<stdin>", cmd.c_str(), cmd.size());
                }
                catch (ell::Error & e)
                {
                    std::cerr << e.what();
                    stack.clear();
                    continue;
                }
            }

            // Print result
            std::cout << pop();
        }
    }

private:
    double pop()
    {
        double n = stack.back();
        stack.pop_back();
        return n;
    }

    void push(double n)
    {
        stack.push_back(n);
    }
    
    std::pair<double,double> pop2()
    {
        double n2 = stack.back(); stack.pop_back();
        double n1 = stack.back(); stack.pop_back();
        return {n1,n2};
    }

    void mul() { auto [a,b] = pop2(); push(a*b); }
    void div() { auto [a,b] = pop2(); push(a/b); }
    void add() { auto [a,b] = pop2(); push(a+b); }
    void sub() { auto [a,b] = pop2(); push(a-b); }
    void neg() { push(-pop()); }

    ell::Rule root, expression, term, factor, blank;

    std::vector<double> stack;
};
