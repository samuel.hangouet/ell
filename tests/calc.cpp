#include "test.h"
#include "Calculator.h"

int main(int argc, const char ** argv)
{
    bool debugMode = false;
    bool interactiveMode = false;

    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if (arg == "-d")
        {
            debugMode = true;
            continue;
        }
        else if (arg == "-i")
        {
            interactiveMode = true;
            continue;
        }

        std::cerr << "Unknown option:" << arg << "\n";
        std::cerr << "Usage: calc [-d|-i] [<input_file>]\n";
        return 1;
    }

    Calculator c;
    c.showTrace(debugMode);
    if (interactiveMode)
    {
        c.loop(debugMode);
        return 0;
    }
    
    // Run tests
    ELL_TEST_EQUAL(c.eval(" 1 +\t4 "), 5);
    
    return 0;
}
