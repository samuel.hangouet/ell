// Example of contextual grammar using dynamic repeat operator and variable assignation operator.
#include "test.h"
#include <ell/Grammar.h>

struct KiwiCounter : public ell::Grammar
{
    unsigned int count = 0;

    KiwiCounter()
    {
        root = skip(ch(' '), ( udec[count] >> str("kiwi") * count
                             | repeat<3, 3>(str("kiwi"))
                             ) >> eos);
    }

    ell::Rule root;
};

int main(int argc, const char ** argv)
{
    KiwiCounter k;
    ell::Parser p;
    p.showTrace();

    ELL_TEST_ERROR(ELL_PARSE(p,k.root,"3 kiwis"));
    ELL_TEST_ERROR(ELL_PARSE(p,k.root,"1 kiwis"));
    ELL_TEST(ELL_PARSE(p,k.root,"1 kiwi"));
    ELL_TEST_ERROR(ELL_PARSE(p,k.root,"0 kiwi"));
    ELL_TEST(ELL_PARSE(p,k.root,"0 "));
    ELL_TEST_ERROR(ELL_PARSE(p,k.root,"2 kiwi kiwis"));
    ELL_TEST(ELL_PARSE(p,k.root,"3 kiwi kiwi kiwi"));
    ELL_TEST_ERROR(ELL_PARSE(p,k.root,"-3 kiwikiwikiwi")); // Minus sign not parse, as type of count is unsigned
    ELL_TEST(ELL_PARSE(p,k.root,"kiwi kiwi kiwi"));
    return 0;
}
