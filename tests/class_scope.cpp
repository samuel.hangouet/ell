#include "test.h"
#include "ell/Grammar.h"

struct ClassScopeGrammar : public ell::Grammar
{
    using rule = ell::Rule;

    rule word = noskip(nosfx(str("cheese") | str("cake"), chset("a-zA-Z0-9")));
    rule root = skip(ch(' '), +word >> eos);
};

int main(int argc, const char ** argv)
{
    ClassScopeGrammar csg;
    ell::Parser p;

    ELL_TEST(ELL_PARSE(p, csg.root, "cheese cheese cake"));
    ELL_TEST_ERROR(ELL_PARSE(p, csg.root, "cheesecake"));
    return 0;
}
