MODE = DEBUG
MODES = DEBUG RELEASE
ifeq ($(strip $(filter $(MODE),$(MODES))),)
$(error Unknown mode $(MODE). Choose in $(MODES))
endif
PFX = build/$(MODE)

INCLUDE = -I.
CXX = g++

DEBUG_CFLAGS = -O0 -g -D_GLIBCXX_DEBUG
RELEASE_CFLAGS = -O3 -DNDEBUG -fomit-frame-pointer -DELL_DEBUG=0

DEBUG_LDFLAGS =
RELEASE_LDFLAGS = -s

CFLAGS = $(INCLUDE) -std=c++20 -Wall -Werror $($(MODE)_CFLAGS)
LDFLAGS = $($(MODE)_LDFLAGS)

TESTS = $(addprefix $(PFX)/,$(basename $(notdir $(wildcard tests/*.cpp))))

test: $(addsuffix .log,$(TESTS))

$(TESTS): $(PFX)/%: tests/%.cpp ell.h tests/*.h
	@mkdir -p $(shell dirname $@)
	$(CXX) -o $@ $< $(LDFLAGS) $(CFLAGS)

$(addsuffix .log,$(TESTS)): $(PFX)/%.log: $(PFX)/% doesnotexist
	@echo -n $* "\t"
	@./$< 2> $@
	@echo OK

ell.h: scripts/make_single_header.sh $(wildcard ell/*.h) $(MAKEFILE_LIST)
	$< > $@

clean:
	rm -rf build

.PHONY: clean test doesnotexist
