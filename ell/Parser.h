#pragma once
#include "NodeBase.h"
#include "Utils.h"
#include "Error.h"
#include "Location.h"
#include <map>
#include <iomanip>
#ifndef ELL_DEBUG
#define ELL_DEBUG 1
#endif
namespace ell
{
    // Every parser extends this class
    struct Parser
    {
        virtual ~Parser() { }

        void match(const NodeBase & root, const char * filename,
                   const char * content, size_t length,
                   int line = 1, int column = 1,
                   Location * parent = nullptr)
        {
            _level = 0;
            _location = { filename, content, line, column, parent };
            _begin = content;
            _end = content + length;
            if (! root.match(this))
                raise_error(&root);
        }

        // Returns current location
        const Location & location() const { return _location; }
        
        // Sets location
        void setLocation(const Location & location) { _location = location; }

        // If build with ELL_DEBUG==1, you can set this flag to show or hide the trace
        void showTrace(bool show = true) { _showTrace = show; }

        // Returns the number of remaining characters to parse
        int remaining() const { return _end - position(); }

        // Returns the current position in buffer being parsed
        const char * position() const { return _location.position; }

        // Returns pointer after the last character to parse
        const char * end() const { return _end; }

        // Consumes a character from parsed buffer and update current location
        void next()
        {
            if (* position() == '\n')
            {
                _location.column = 1;
                _location.line++;
            }
            else
                _location.column++;

            _location.position++;
        }

        // Consumes n characters
        void next(int n)
        {
            for (int i = 0; i < n; ++i)
                next();
        }

        // Same but faster than next() when the next n characters cannot be a newline
        void sameLineNext(int n)
        {
            _location.column += n;
            _location.position += n;
        }

        // Throws an parsing error using given node and current buffer position to build the message
        void raise_error(const NodeBase * mismatch, const char * message=nullptr) const
        {
            std::ostringstream os;
            os << _location << ": error: ";
            if (mismatch != nullptr)
            {
                os << "expecting ";
                mismatch->describe(os);
                os << " before ";
                dumpPosition(os);
            }

            if (message != nullptr)
            {
                if (mismatch == nullptr)
                {
                    os << "at ";
                    dumpPosition(os);
                }
                os << ": " << message;
            }

            throw Error(os.str());
        }

        // If a skipper has been set, tries to advance among buffer matching it
        void trySkip()
        {
            if (_skipper == nullptr)
                return;
            const NodeBase * skipper = _skipper;
            SafeModify<const NodeBase*> ms(_skipper, nullptr);
#if ELL_DEBUG == 1
            SafeModify<bool> mt(_showTrace, false);
#endif
            while (skipper->match(this));
        }

        // Dumps current position of buffer being parsed
        void dumpPosition(std::ostream & os) const
        {
            if (remaining() == 0)
            {
                os << "end";
                return;
            }

            os << '"';
            for(int i = 0; i < remaining(); ++i)
            {
                char c = * (position() + i);
                ell::dump_char(os, c);
                if (c == '\n')
                    // Do not cross end of line
                    break;
                if (i == 19)
                {
                    os << "\"...";
                    return;
                }
            }
            os << '"';
        }

        void traceBegin(const NodeBase * node)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                trace(node, "\\");
                _level++;
            }
        }

        bool traceEnd(const NodeBase * node, bool match)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                _level--;
                trace(node, match ? "/" : "#");
            }
            return match;
        }

        // Can also be called from a semantic action
        void trace(const NodeBase * node = nullptr, const char * flag = nullptr, const char * msg = nullptr)
        {
            if constexpr (ELL_DEBUG == 1)
            {
                if (! _showTrace)
                    return;
                traceIndent();
                if (flag != nullptr)
                    std::cerr << flag << ' ';
                if (msg != nullptr)
                    std::cerr << msg << ' ';
                if (node != nullptr)
                {
                    node->dump(std::cerr);
                    std::cerr << ' ';
                }
                std::cerr << '\t';
                dumpPosition(std::cerr);
                std::cerr << std::endl;
            }
        }

        // Dumps white spaces according to current indentation level of trace
        void traceIndent()
        {
            if constexpr (ELL_DEBUG == 1)
                std::cerr << std::string(_level, ' ');
        }

    private:
#if ELL_DEBUG == 1
        bool _showTrace = false;
        int _level = 0;
#endif

        const char * _begin = nullptr;
        const char * _end = nullptr;

        Location _location = {};

        const NodeBase * _skipper = nullptr;
        template <typename Left,typename Right> friend struct SkipNode;
        template <typename Child> friend struct NoSkipNode;

        bool _fallback = false;
        template <typename Child> friend struct FallbackNode;
        template <typename Left,typename Right> friend struct SequenceNode;
    };
}
