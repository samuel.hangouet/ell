#pragma once
#include "NodeChild.h"
#include <vector>
#include <cstring>
namespace ell
{
    // Abstract class for grammar binary nodes
    template <typename ConcreteNode, typename Left, typename Right>
    struct BinaryNodeBase : public ConcreteNodeBase<ConcreteNode>
    {
        NodeChild<Left> left;
        NodeChild<Right> right;

    protected:
        BinaryNodeBase(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : left(left.cast()), right(right.cast())
        { }
    };

    // Operator >>
    template <typename Left, typename Right>
    struct SequenceNode final : public BinaryNodeBase<SequenceNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<SequenceNode<Left,Right>>::match;

        SequenceNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<SequenceNode<Left,Right>,Left,Right>(left, right)
        { }

        void dump(std::ostream & os) const override { os << this->left << ' ' << this->right; }
        void describe(std::ostream & os) const override { os << this->left; }

        bool match(Parser * p) const
        {
            Location begin = p->location();

            if (this->left.node.match(p))
            {
                p->trySkip();

                if (this->right.node.match(p))
                    return true;

                if (! p->_fallback)
                    p->raise_error(& this->right.node);
            }

            p->setLocation(begin);
            return false;
        }
    };

    template <typename Left, typename Right>
    const SequenceNode<Left, Right> operator >> (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return SequenceNode<Left, Right>(left, right);
    }

    // Operator %
    template <typename Left, typename Right>
    struct ListNode final : public BinaryNodeBase<ListNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ListNode<Left,Right>>::match;

        ListNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ListNode<Left,Right>,Left,Right>(left, right)
        { }

        void describe(std::ostream & os) const override { os << this->left << " separated by " << this->right; }

        bool match(Parser * p) const
        {
            bool matched = false;
            Location begin = p->location();

            while (this->left.node.match(p))
            {
                matched = true;
                begin = p->location();
                p->trySkip();
                if (! this->right.node.match(p))
                    break;
                p->trySkip();
            }

            p->setLocation(begin);
            return matched;
        }
    };

    template <typename Left, typename Right>
    const ListNode<Left, Right> operator % (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ListNode<Left, Right>(left, right);
    }

    // Operator |
    template <typename Left, typename Right>
    struct ChoiceNode final : public BinaryNodeBase<ChoiceNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ChoiceNode<Left,Right>>::match;

        void describe(std::ostream & os) const override { os << this->left << " or " << this->right; }

        ChoiceNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ChoiceNode<Left,Right>,Left,Right>(left, right)
        { }

        bool match(Parser * p) const
        {
            return this->left.node.match(p) || this->right.node.match(p);
        }
    };

    template <typename Left, typename Right>
    const ChoiceNode<Left,Right> operator | (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ChoiceNode<Left, Right>(left, right);
    }

    // Checks that a node is not followed by another one without consuming it, and without skipping
    // Used to build identifiers rules. Ex: keyword = nosfx(str("keyword"), chset("a-zA-Z_0-9"))
    template <typename Left, typename Right>
    struct NoSuffixNode final : public BinaryNodeBase<NoSuffixNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<NoSuffixNode<Left,Right>>::match;

        NoSuffixNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<NoSuffixNode<Left,Right>,Left,Right>(left, right)
        { }

        void describe(std::ostream & os) const override { os << this->left; }

        bool match(Parser * p) const
        {
            Location begin = p->location();
            if (! this->left.node.match(p))
                return false;

            // No skip in here

            if (! this->right.node.match(p))
                return true;

            p->setLocation(begin);
            return false;
        }
    };

    // Exclusion operator (-)
    template <typename Left, typename Right>
    struct ExclusionNode final : public BinaryNodeBase<ExclusionNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<ExclusionNode<Left,Right>>::match;

        ExclusionNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<ExclusionNode<Left,Right>,Left,Right>(left, right)
        { }

        void describe(std::ostream & os) const override { os << this->left << " except " << this->right; }

        bool match(Parser * p) const
        {
            p->traceBegin(this);

            Location begin = p->location();
            if (this->right.node.match(p))
            {
                p->setLocation(begin);
                return p->traceEnd(this, false);
            }

            return p->traceEnd(this, this->left.node.match(p));
        }
    };

    template <typename Left, typename Right>
    const ExclusionNode<Left,Right> operator - (const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
    {
        return ExclusionNode<Left, Right>(left, right);
    }

    // Skipper setting
    template <typename Left, typename Right>
    struct SkipNode final : public BinaryNodeBase<SkipNode<Left,Right>,Left,Right>
    {
        using ConcreteNodeBase<SkipNode<Left,Right>>::match;

        SkipNode(const ConcreteNodeBase<Left> & left, const ConcreteNodeBase<Right> & right)
          : BinaryNodeBase<SkipNode<Left,Right>,Left,Right>(left, right)
        { }

        void dump(std::ostream & os) const override { os << "skip(" << this->left << ", " << this->right << ")"; }
        void describe(std::ostream & os) const override { os << this->right; }

        bool match(Parser * p) const
        {
            p->traceBegin(this);
            SafeModify<const NodeBase*> ms(p->_skipper, & this->left.node);
            p->trySkip();
            return p->traceEnd(this, this->right.node.match(p));
        }
    };
}
