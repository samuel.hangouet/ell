#pragma once
#include "Rule.h"
namespace ell
{
    // Class used to handle rule children by ref and other nodes by value
    template <typename Child>
    struct NodeChild
    {
        NodeChild(const ConcreteNodeBase<Child> & n)
          : node(n.cast())
        { }

        Child node;
    };

    template <>
    struct NodeChild<Rule>
    {
        NodeChild(const ConcreteNodeBase<Rule> & n)
          : node(n.cast())
        { }

        const Rule & node;
    };

    template <typename Child>
    inline std::ostream & operator << (std::ostream & os, const NodeChild<Child> & n)
    {
        n.node.describe(os);
        return os;
    }
}
